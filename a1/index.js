// DOM - document object model

// querySelector function takes a string input that is formatted like CSS selector when applying styles. This allows to get a specific element


// console.log(document)

// alternative
document.getElementById("txt-first-name")



const txtFirstName = document.querySelector('#txt-first-name')
const txtLasttName = document.querySelector('#txt-last-name')
const spanFullName = document.querySelector('#span-full-name')

const updateFullName = () => {
	let FirstName = txtFirstName.value
	let LastName = txtLasttName.value

	spanFullName.innerHTML = `${FirstName} ${LastName}`
}



	txtFirstName.addEventListener('keyup', updateFullName)

	txtlasttName.addEventListener('keyup', updateFullName)

// function myFunction() {
//   var x = document.getElementById("fname").value;
//   document.getElementById("demo").innerHTML = x;
// }
